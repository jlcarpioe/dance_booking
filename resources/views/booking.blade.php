@extends('layouts.app')

@section('content')
<div class="content">
    <div class="title m-b-md">
        {{ config('app.name', 'Project') }}
    </div>

    <div class="container">
        <dateselect-component></dateselect-component>
    </div>
</div>
@endsection
