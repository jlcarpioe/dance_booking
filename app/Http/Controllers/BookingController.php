<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;


class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['info' => Reservation::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('booking');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        // validation
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'appointment_date' => [
                'required', 
                'date_format:"Y-m-d"',
                function ($attribute, $value, $fail) {
                    $date = Carbon::parse($value);
                    $day = $date->isoFormat('d');
                    if ( $day < 1 || $day > 5) {
                        $fail('The ' . $attribute . ' must be between Monday and Friday');
                    }
                },
            ],
            'appointment_time' => [
                'required',
                'date_format:"H"',
                function ($attribute, $value, $fail) {
                    if ( $value < 9 || $value > 18) {
                        $fail('The ' . $attribute . ' must be between 9 and 16 hours');
                    }
                },
                Rule::unique('reservations')->where(function($query) use ($request) 
                {
                    return $query->where('appointment_date', $request['appointment_date']);
                }),
            ],
        ]);

        if ($validator->fails()) {
            return response()->json(['error_code' => 147, 'error_message' => $validator->errors()->first()], 400);
        }

        return response()->json(['info' => Reservation::create($request->all())]);
    }


    /**
     * Display a listing of the resource.
     *
     * @param  string $date
     * @return \Illuminate\Http\Response
     */
    public function search($date)
    {
        return response()->json(['info' => Reservation::where('appointment_date', $date)->get()]);
    }
}
