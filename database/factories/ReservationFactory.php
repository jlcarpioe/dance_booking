<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Reservation;
use Faker\Generator as Faker;


$factory->define(Reservation::class, function (Faker $faker) {
    return [
        'appointment_date' => '2019-08-20',
        'appointment_time' => $faker->unique()->numberBetween(9, 18),
        'email' => $faker->unique()->safeEmail,
    ];
});
