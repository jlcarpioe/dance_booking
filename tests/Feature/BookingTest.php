<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingTest extends TestCase
{
    use WithFaker;


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreate()
    {
        
        // $response = $this->post('api/booking', ['email' => $this->faker->safeEmail, 'appointment_date' => '2019-08-20', 'appointment_time' => '12']);

        $response = $this->post('api/booking', ['email' => $this->faker->safeEmail, 'appointment_date' => '2019-08-20', 'appointment_time' => '09']);
        
        $response->dump();

        // $response->assertStatus(400);
        // $response->assertJsonStructure(['error_code', 'error_message']);

        $response->assertStatus(200);
        $response->assertJsonStructure(['data' => ['email', 'appointment_date', 'appointment_time', 'id']]);
    }
}
